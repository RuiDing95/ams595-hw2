function [L,U,P] = LU_decomposition(A)
% function for part 2(c)
%Using the code for forward elimination with elementary matrices products returned 
%separately for row-exchange operations (P) and for other operations (Q)
%input: matrix A
%output: decomposition L,U,P such that PA=LU
i=1;
j=1;
A_original = A;
M = length(A(1,:));
N = length(A(:,1));
P = eye(N);
Q = eye(N);
while i<N && ~all(A(i,:)==0)
    if A(i,j)~=0
        for k=i+1:N
            
            cur = eye(N);
            cur(k,i) = -A(k,j)/A(i,j);
            %other operations multiply to Q
            Q = cur*Q;
            A(k,:) = A(k,:)-A(i,:)*A(k,j)/(A(i,j));
        end
        i=i+1;
        j=j+1;
    else
        free=1;
        for k=i+1:N
            if A(k,j)~=0
                free=0;
                %row-exchange operations multiply to P
                cur=eye(N);
                cur(i,i)=0;
                cur(k,k)=0;
                cur(i,k)=1;
                cur(k,i)=1;
                P = cur*P;
                temp=A(i,:);
                A(i,:)=A(k,:);
                A(k,:) = temp;
                break;
            end
        end
        if free==0
            for k=i+1:N
                %other operations multiply to Q
                cur = eye(N);
                cur(k,i) = -A(k,j)/A(i,j);
                Q = cur*Q;
                A(k,:) = A(k,:)-A(i,:)*A(k,j)/(A(i,j));
            end
            i=i+1;
            j=j+1;
        else
            j=j+1;
        end
    end
end
%define L,U, where L calculated using the inverse function from part 2(b)
U = Q*A_original;
L = inverse(Q,"matrix","matrix");

end