function z = form_test(A)
%function for part 1(a)
%input: matrix-A
%output: string - form
if row_canonical(A)
    %fprintf('The matrix A is in row canonical form.');
    z='row canonical';
elseif echelon(A)
    %fprintf('The matrix A is in echelon form.');
    z='echelon';
else
    %fprintf('The matrix is neither.');
    z='neither';
end
end

function y=echelon(A)
%function for testing if A is echelon form
%input:matrix-A
%output:boolean value true/false
y = true;
M = length(A(1,:));
N = length(A(:,1));
i=1;
p=1;
for j=1:M
    if A(i,j)~=0
        p=j;
        break;
    end
end

for i = 2:N
    for j = 1:M
        if A(i,j)~=0
            
            break;
        end
    end
    if j<=p
        y=false;
        break;
    else
        p=j;
    end
    
end
end

function y=row_canonical(A)
%function for testing if A is canonical form
%input:matrix-A
%output:boolean value true/false
y = true;
M = length(A(1,:));
N = length(A(:,1));
i=1;
p=1;
for j=1:M
    if A(i,j)~=0
        p=j;
        break;
    end
end
if A(i,j)~=1
    y=false;
else

    for i = 2:N
        j=1;
        while j<=M
            if A(i,j)~=0
                break;
            end
            j=j+1;
        end
        if j>M
            p=j;
        elseif j<=p || A(i,j)~=1 || sum(A(:,j)~=0)~=1
            y=false;
            break;
        else
            p=j;
        end

    end
end
end



            
    