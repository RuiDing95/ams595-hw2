function sol = solve(A,b)
%function for part 1(d)
%input: matrix-A, column vector-b
%output: solution vector
M = length(A(1,:));
N = length(A(:,1));
if length(B)~=N
    error("Input dimensions mismatch");
    sol=[];
end

%get the row-canonical form of augmented matrix C
C_echelon = forward(C);
R = backward(C_echelon);
i=N;
sol=zeros(M,1);
while i>=1
    %if there exists free variables, then system has infinite solutions
    if sum(R(i,1:M)~=0)>1
        sol=[];
        error('The system has infinitely many solutions.');
        break;
    %if system has contracdictory equations, then there's no solutions
    elseif sum(R(i,1:M)~=0)<1 && R(i,M+1)~=0
        sol=[];
        error('The system has no solutions.');
        break;
    %skip zero rows
    elseif sum(R(i,1:M)~=0)<1 && R(i,M+1)==0
        i=i-1;
    else
        k=1;
        for j = 1:M
            if R(i,j)==1
                k=j;
            end
        end
        sol(k)=R(i,M+1);
        i=i-1;
    end
end

    
                
        
        
