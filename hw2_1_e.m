%Monte carlo sampling with 1000 samples for every pair of m,n
N = 1000;
R=10;
Z = zeros(1,R*R);
X = zeros(1,R*R);
Y=zeros(1,R*R);
for n=1:R
    for m=1:R
        T=0;
        %record each setting of m and n
        X(n*R-R+m) = m;
        Y(n*R-R+m) = n;
        tic;
        for i=1:N
            %create random matrices with elements in [0,10]
            A = randi([0,10],m,n);
            b=randi([0,10],m,1);
            try
                sol=solve(A,b);
                t = toc;
                T = T+t;
            catch
                t=toc;
                T = T+t;
            end
        end
        %count average execution time
        Z(n*R-R+m)=T/N;
    end
end
%plot 3D
plot3(X,Y,Z,'o','MarkerFaceColor','r');
xlabel('m');
ylabel('n');
zlabel('Computation time');
title(strcat('Samples per parameter set: ',string(N)));