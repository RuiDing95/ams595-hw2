function output = inverse(f,option_in,option_out)
% function for part 2(b)
%input: f-file name or matrix
%option_in: "text" or "matrix"
%option_out: "text" or "matrix"
if option_in=="text"
    %read matrix from txt file 
    A = dlmread(f);
elseif option_in=="matrix"
    A = f;
else
    error('Invalid input type.');
    output=[];
end
%results from forward and backward eliminations
[E,P] = forward_elementary(A);
[R,Q] = backward_elementary(E);
%calculate inverse of A by multiplying all elementary matrices
A_inv = Q*P;
M = length(A_inv(1,:));
N = length(A_inv(:,1));
if M~=N
    error('Input matrix is not a square matrix.');
    output=[];
%check if matrix is non-singular
elseif all(all(R==eye(N)))
    if option_out=="text"
        %write the inverse matrix to a specific txt file
        dlmwrite('matrix_inverse.txt',A_inv);
        output="matrix_inverse.txt";
    elseif option_out=="matrix"
        output=A_inv;
    else
        error('Invalid output type.');
        output=[];
    end
else
    error('Input matrix is singular.');
    output=[];
end

        
    
end
