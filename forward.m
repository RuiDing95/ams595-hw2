
function E = forward(A)
%function for part 1(b)
%input: matrix-A
%output: echelon form-E
i=1;
j=1;
M = length(A(1,:));
N = length(A(:,1));
while i<N && ~all(A(i,:)==0)
    %step 2
    if A(i,j)~=0
        %step 5
        for k=i+1:N
            A(k,:) = A(k,:)-A(i,:)*A(k,j)/(A(i,j));
        end
        i=i+1;
        j=j+1;
    else
        free=1;
        %step 3
        for k=i+1:N
            if A(k,j)~=0
                free=0;
                temp=A(i,:);
                A(i,:)=A(k,:);
                A(k,:) = temp;
                break;
            end
        end
        %go to step 5
        if free==0
            for k=i+1:N
                A(k,:) = A(k,:)-A(i,:)*A(k,j)/(A(i,j));
            end
            i=i+1;
            j=j+1;
        %step 4
        else
            j=j+1;
        end
    end
end
            
E=A;               
end

