function R = backward(A)
%function for part 1(c)
%input: matrix-A
%output: canonical form-R
if form_test(A)=="neither"
    msg = 'The input matrix is not in row echelon form.';
    error(msg);
    R=[];
else
    M = length(A(1,:));
    N = length(A(:,1));
    i = N;
    j=1;
    %Step 1 in back-substitution
    while all(A(i,:)==0)
        i=i-1;
    end
    while A(i,j)==0
        j=j+1;
    end
    %step 2
    while i~=1
        A(i,:) = A(i,:)/A(i,j);
        %step 3
        for k = 1:i-1
           A(k,:) = A(k,:)-A(i,:)*A(k,j);
        end
        i=i-1;
        j=1;
        while A(i,j)==0
            j=j+1;
        end
    end
    A(i,:) = A(i,:)/A(i,j);
    R = A;
end

end

