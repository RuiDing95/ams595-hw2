function [E,P] = forward_elementary(A)
%function for part 2(a)
%input: input matrix - A
%output: product of elementary operations matrices - P
%row-echelon form - E such that PA=E
i=1;
j=1;
M = length(A(1,:));
N = length(A(:,1));
P = eye(N);
while i<N && ~all(A(i,:)==0)
    %step 1 in forward elimination
    if A(i,j)~=0
        %step 5
        for k=i+1:N
            %elementary matrix for addition
            cur = eye(N);
            cur(k,i) = -A(k,j)/A(i,j);
           
            P = cur*P;
            A(k,:) = A(k,:)-A(i,:)*A(k,j)/(A(i,j));
        end
        i=i+1;
        j=j+1;
    else
        free=1;
        %step 3
        for k=i+1:N
            if A(k,j)~=0
                free=0;
               
                cur=eye(N);
                cur(i,i)=0;
                cur(k,k)=0;
                cur(i,k)=1;
                cur(k,i)=1;
                P = cur*P;
                temp=A(i,:);
                A(i,:)=A(k,:);
                A(k,:) = temp;
                break;
            end
        end
        %if new pivot is found, do step 5
        if free==0
            for k=i+1:N
               
                cur = eye(N);
                cur(k,i) = -A(k,j)/A(i,j);
                P = cur*P;
                A(k,:) = A(k,:)-A(i,:)*A(k,j)/(A(i,j));
            end
            i=i+1;
            j=j+1;
        else
            %if this column is a free variable, do step 4
            j=j+1;
        end
    end
end
%return the row-echelon form matrix E           
E=A;               
end

