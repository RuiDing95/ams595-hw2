function [R,P] = backward_elementary(A)
%function for part 2(a)
%input: echelon form matrix - A
%output: product of elementary operations matrices - P
%row-canonical form - R such that PA=R
if form_test(A)=="neither"
    %throw error if input not in echelon form
    msg = 'The input matrix is not in row echelon form.';
    error(msg);
    R=[];
else
    M = length(A(1,:));
    N = length(A(:,1));
    i = N;
    j=1;
    P = eye(N);
    %Step 1 in back-substitution
    while all(A(i,:)==0)
        i=i-1;
    end
    while A(i,j)==0
        j=j+1;
    end
    %step 2
    while i~=1
        %the elementary matrix for row multiplication
        cur=eye(N);
        cur(i,i) = 1/A(i,j);
        P = cur*P;
        A(i,:) = A(i,:)/A(i,j);
        %step 3
        for k = 1:i-1
           %the elementary matrix for row addition
           cur = eye(N);
           cur(k,i) = -A(k,j);   
           P = cur*P;
           A(k,:) = A(k,:)-A(i,:)*A(k,j);
        end
        i=i-1;
        j=1;
        while A(i,j)==0
            j=j+1;
        end
    end
    cur=eye(N);
    cur(i,i) = 1/A(i,j);
    P = cur*P;
    A(i,:) = A(i,:)/A(i,j);
    %the result is a row-canonical form
    R = A;
end

end
